import { Usluga } from './../Usluga.service';
import { Http, Response } from '@angular/http';

import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-home-scren',
  templateUrl: './home-scren.component.html',
  styleUrls: ['./home-scren.component.css']
})
export class HomeScrenComponent implements OnInit {

constructor(private http: Http, private service: Usluga){}


  BrKoraka: number = 0;
  PotrosenoKalorija: Number;
  Vreme: number;
  Dan:String;
  Sat: Number;
  Minuti: Number;
  Datum: any;

  ngOnInit() {
    
        this.http.get('https://api.myjson.com/bins/1gwnal?fbclid=IwAR1jM8e48gXZylO9n3KzN1Dyxbqosi7y1vRSd9p9XMnrvUKPspiS0y0_lyo/data.json')
      .subscribe((res: Response) => {
          const data = res.json();
          
                data.forEach((el, i) => {
                this.BrKoraka = this.BrKoraka + el.steps;
              })

          /* Ukupno Kalorija Potroseno */    
          this.PotrosenoKalorija = Math.round(this.BrKoraka * 0.05);

          /* Prosecno sati i minuta */
          this.Vreme = Math.round(this.BrKoraka * 0.5)/5;
          const Secundi:number = this.Vreme;
          this.Sat = Math.floor(Secundi / 3600);
          this.Minuti = Math.floor(this.Vreme % 3600 / 60);

      })

  }

  onIzabranDan(event: String){
    this.service.DanIzbran = event;
  }


}
