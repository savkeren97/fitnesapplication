import { Component, OnInit, Output, EventEmitter } from '@angular/core';




@Component({
  selector: 'app-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.css']
})
export class DaysComponent implements OnInit {

  nizDana: String[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  @Output() izabranDan: EventEmitter<String> = new EventEmitter();


  

  constructor() { }

  ngOnInit() {
  
  }

  

  onDayClicked(Dan: String){
    this.nizDana.forEach(element => {
      if(Dan === element){
        this.izabranDan.emit(Dan);
      }
    });
    
  }

 

}
